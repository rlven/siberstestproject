import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

export class FormPersonalDetails extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { values, handleChange } = this.props;
    return (

        <Dialog 
            open="true"
            fullWidth="true"
            maxWidth='sm'
          >
            <AppBar title="Enter Personal Details" />

            <TextField
              placeholder="Enter company customer"
              label="Company customer"
              onChange={handleChange('projectCustomer')}
              defaultValue={values.projectCustomer}
              margin="normal"
							fullWidth="true"
            />
            <br />
              <TextField
              placeholder="Enter company supervisor"
              label="Company supervisor"
              onChange={handleChange('projectSupervisor')}
              defaultValue={values.projectSupervisor}
              margin="normal"
              fullWidth="true"
            />
            <br />
            <TextField
              placeholder="Enter project executor"
              label="Project executor"
              onChange={handleChange('projectExecutor')}
              defaultValue={values.projectExecutor}
              margin="normal"
							fullWidth="true"
            />
            <br />
        
            <Button
              color="secondary"
              variant="contained"
              onClick={this.back}
            >Back</Button>

            <Button
              color="primary"
              variant="contained"
              onClick={this.continue}
            >Continue</Button>
          </Dialog>
    );
  }
}

export default FormPersonalDetails;

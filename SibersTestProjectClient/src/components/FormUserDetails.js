import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

export class FormUserDetails extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  render() {
    const { values, handleChange } = this.props;
    return (

          <Dialog 
            open="true"
            fullWidth="true"
            maxWidth='sm'
          >

            <TextField
              placeholder="Enter project name"
              label="Project name"
              onChange={handleChange('projectName')}
              defaultValue={values.projectName}
              margin="normal"
							fullWidth="true"
            />
            <br />
            <TextField
              label="Project start date"
              onChange={handleChange('projectStartDate')}
              defaultValue={values.projectStartDate}
              margin="normal"
							fullWidth="true"
              type="date"
                  InputLabelProps={{
                    shrink: true,
                  }}
            />
            <br />
            <TextField
              label="Project end date"
              onChange={handleChange('projectEndDate')}
              defaultValue={values.projectEndDate}
              margin="normal"
              fullWidth="true"
              type="date"
                  InputLabelProps={{
                    shrink: true,
                  }}
            />
            <br />
            <Button
              color="primary"
              variant="contained"
              onClick={this.continue}
            >Continue</Button>
          </Dialog>

    );
  }
}

export default FormUserDetails;

import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import { List, ListItem, ListItemText } from '@material-ui/core/';
import Button from '@material-ui/core/Button';

export class Confirm extends Component {
  continue = e => {
    e.preventDefault();
    // PROCESS FORM //
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const {
      values: {projectName, projectStartDate, projectEndDate, projectPriority, projectCustomer, projectExecutor, projectSupervisor}
    } = this.props;
    return (
        <React.Fragment>
          <Dialog
            open="true"
            fullWidth="true"
            maxWidth='sm'
          >

          <List>
            <ListItem>
              <ListItemText primary="Project name" secondary={projectName} /> 
            </ListItem>
            <ListItem>
              <ListItemText primary="Project start date" secondary={projectStartDate} /> 
            </ListItem>
            <ListItem>
              <ListItemText primary="Project end date" secondary={projectEndDate} /> 
            </ListItem>
            <ListItem>
              <ListItemText primary="Porject customer" secondary={projectCustomer} /> 
            </ListItem>
            <ListItem>
              <ListItemText primary="Project executor" secondary={projectExecutor} /> 
            </ListItem>
            <ListItem>
              <ListItemText primary="Project supervisor" secondary={projectSupervisor} /> 
            </ListItem>
          </List>
          <br />
        
          <Button
            color="secondary"
            variant="contained"
            onClick={this.back}
          >Back</Button>

          <Button
            color="primary"
            variant="contained"
            onClick={this.continue}
          >Confirm & Continue</Button>
          </Dialog>
        </React.Fragment>
    );
  }
}

export default Confirm;

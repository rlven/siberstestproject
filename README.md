Sibers test project
## Asp net mvc core (Админ панель)

Asp net core 3 mvc приложение:Используемые библиотеки
-EntityFrameworkCore
-Moq
-EntityFrameworkCore.SqlServer
-AspnetCoreIdentity
-EntityFrameworkCore.Tools
-Bootstrap

Приложение разбито на несколько уровней
-Domain(уровень доступа к данным)
-Infrastructure(реализация репозиториев, миграции)
-Application(бизнес логика)
-UnitTest(unit тесты)
-SibersTestProj(само приложение(админ панель))

## React(клиентское приложение)

Клиентское приложение (Доп задание 2) реализована на React, используемые библиотеки

-material-ui-core

Для запуска приложения react необходимо выполнить команды для установки библиотек
-npm install .
-npm install @material-ui/core



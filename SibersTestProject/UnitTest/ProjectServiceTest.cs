﻿using Application.Services.Implementations;
using Domain.Entities;
using Domain.Interfaces;
using Infrastructure.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTest
{
    [TestFixture]
    public class ProjectServiceTest
    {
        private Mock<IUnitOfWork> mockRepo;
        private ProjectService projectService;
        private List<Project> projects;
        public List<Project> Projects { get => projects; set => projects = value; }
        [SetUp]
        public void Setup()
        {
            mockRepo = new Mock<IUnitOfWork>();
            Projects = new List<Project>();
            Projects.Add(new Project() { Id = 1, Name = "test", CustomerName = "test", ExecutorName = "test", ProjectPriority = 1 });
            Projects.Add(new Project() { Id = 2, Name = "test2", CustomerName = "test2", ExecutorName = "test2", ProjectPriority = 2 });
            Projects.Add(new Project() { Id = 3, Name = "test3", CustomerName = "test3", ExecutorName = "test3", ProjectPriority = 3 });
            mockRepo.Setup(p => p.ProjectRepository.All).Returns(Projects.AsQueryable());
            projectService = new ProjectService(mockRepo.Object);
        }

        [Test]
        public void CreateProjectTest() 
        {
            var project = new ProjectCreateEditModel() { Name = "test", Customer = "test", Executor = "test", Priority = 1 };
            var createdProject = new Project() {Name = "test", CustomerName = "test", ExecutorName = "test", ProjectPriority = 1 };

            projectService.CreateProjectAsync(project).GetAwaiter();
            var result = projectService.GetProjectByIdAsync(4);
            Assert.IsNotNull(result);
            Assert.Pass();
        }

        [Test]
        public void GetAllProjects()
        {
            var result = projectService.GetAllProjects();

            Assert.AreEqual(result, projects);
            Assert.Pass();
        }

        [Test]
        public void DeleteProject()
        {
            projectService.DeleteProjectAsync(1).GetAwaiter();
            var result = projectService.GetProjectByIdAsync(1);

            Assert.IsNull(result);
            Assert.Pass();
        }
    }
}

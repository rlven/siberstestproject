﻿using Application.Services.Implementations;
using Application.Services.Interfaces;
using Domain.Entities;
using Domain.Interfaces;
using Infrastructure.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTest
{
    [TestFixture]
    public class EmployeServiceTest
    {
        private Mock<IUnitOfWork> mockRepo;
        private EmployeeService employeeService;
        private List<Employee> employes;

        public List<Employee> Employes { get => employes; set => employes = value; }

        [SetUp]
        public void Setup() 
        {
            mockRepo = new Mock<IUnitOfWork>();
            Employes = new List<Employee>();
            Employes.Add(new Employee() { Id = "test", FirstName = "test", LastName = "test", MiddleName = "test", Email = "test" });
            Employes.Add(new Employee() { Id = "test2", FirstName = "test2", LastName = "test2", MiddleName = "test2", Email = "test2" });
            Employes.Add(new Employee() { Id = "test3", FirstName = "test3", LastName = "test3", MiddleName = "test3", Email = "test3" });
            mockRepo.Setup(p => p.EmployeeRepository.All).Returns(Employes.AsQueryable());
            employeeService = new EmployeeService(mockRepo.Object);
        }

        [Test]
        public void CreateEmployeeTest()
        {
            var employe = new EmployeeCreateModel() { FirstName = "test", LastName = "test", MiddleName = "test", Email = "test" };
            var creaedEmploye = new Employee() { FirstName = "test", LastName = "test", MiddleName = "test", Email = "test" };

            var result = employeeService.CreateEmployee(employe);
            Assert.AreEqual(creaedEmploye.FirstName, result.FirstName);
            Assert.AreEqual(creaedEmploye.Email, result.Email);
            Assert.Pass();
        }
        [Test]
        public void GetAllEmployes()
        {   
            var result = employeeService.GetAllEmployes();

            Assert.AreEqual(result, Employes);
            Assert.Pass();
        }

        [Test]
        public void DeleteEmploye()
        {
            var employe = new EmployeeCreateModel() { Id="test", FirstName = "test", LastName = "test", MiddleName = "test", Email = "test" };

            employeeService.DeleteEmployeeAsync("test").GetAwaiter();
            var result = employeeService.GetEmployeeByIdAsync("test").GetAwaiter();
            
            Assert.IsNull(result.GetResult());
            Assert.Pass();
        }
    }
}

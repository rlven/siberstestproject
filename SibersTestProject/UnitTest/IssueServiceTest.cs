﻿using Application.Services.Implementations;
using Domain.Entities;
using Domain.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    [TestFixture]
    public class IssueServiceTest
    {
        private Mock<IUnitOfWork> mockRepo;
        private IssueService issueService;
        private List<Issue> issues;

        public List<Issue> Issues { get => issues; set => issues = value; }

        [SetUp]
        public void Setup()
        {
            mockRepo = new Mock<IUnitOfWork>();
            Issues = new List<Issue>();
            Issues.Add(new Issue() { Id = 1, Name = "test", Description = "test", IssuePriority = 1, IssueStatusId = 1 });
            Issues.Add(new Issue() { Id = 2, Name = "test2", Description = "test2", IssuePriority = 2, IssueStatusId = 2 });
            Issues.Add(new Issue() { Id = 3, Name = "test3", Description = "test3", IssuePriority = 3, IssueStatusId = 3 });
            mockRepo.Setup(p => p.IssueRepository.All).Returns(Issues.AsQueryable());
            issueService = new IssueService(mockRepo.Object);
        }

        [Test]
        public void CreateIssueTest()
        {
            var issue = new Issue() { Id = 4, Name = "test", Description = "test", IssuePriority = 1, IssueStatusId = 1 };

            issueService.CreateIssueAsync(issue).GetAwaiter();
            var result = issueService.GetIssueByIdAsync(4);
            Assert.IsNotNull(result);
            Assert.Pass();
        }
        [Test]
        public void EditIssue()
        {
            var editingIssue = new Issue() { Id = 1, Name = "editedTest", Description = "editedTest", IssuePriority = 1, IssueStatusId = 1 };
            var result = issueService.EditIssueAsync(editingIssue).GetAwaiter();
            var editedIssue = issueService.GetIssueByIdAsync(1).GetAwaiter();
            Assert.Pass();
        }

        [Test]
        public void DeleteIssue()
        {
            issueService.DeleteIssueAsync(1).GetAwaiter();
            var result = issueService.GetIssueByIdAsync(1).GetAwaiter();

            Assert.IsNull(result.GetResult());
            Assert.Pass();
        }
    }
}

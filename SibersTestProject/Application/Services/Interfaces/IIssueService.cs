﻿using Domain.Entities;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Interfaces
{
    public interface IIssueService
    {
        Task CreateIssueAsync(Issue issue);
        Task EditIssueAsync(Issue issue);
        Task DeleteIssueAsync(int id);
        Task DeleteIssueFromProject(int projectId, int issueId);
        Task AddIssuesToProject(AddIssuesToProjectModel model);
        Task ChangeIssueExecutor(int employeeId, int issueId);
        Task<List<Issue>> GetAllIssuesAsync();
        Task<Issue> GetIssueByIdAsync(int id);
        IQueryable<IssueStatus> GetIssueStatuses();
    }
}

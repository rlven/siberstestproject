﻿using Domain.Entities;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Interfaces
{
    public interface IEmployeeService
    {
        List<Employee> GetAllEmployes();
        Employee CreateEmployee(EmployeeCreateModel model);
        Task EditEmployeeAsync(EmployeeCreateModel model);
        Task DeleteEmployeeAsync(string id);
        Task<Employee> GetEmployeeByIdAsync(string id);
        EmployeeCreateModel GetEmployeeModel(Employee employee);
    }
}

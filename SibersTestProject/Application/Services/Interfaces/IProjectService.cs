﻿using Domain.Entities;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Interfaces
{
    public interface IProjectService
    {
        Task CreateProjectAsync(ProjectCreateEditModel model);
        Task EditProjectAsync(Project model);
        Task DeleteProjectAsync(int id);
        Task AddEmployesToProjectAsync(AddEmployesToProjectModel model);
        List<Project> GetAllProjects();
        Task<Project> GetProjectByIdAsync(int id);
        Task<Project> GetProjectDetailsAsync(int id);
        Task<List<Employee>> GetAvailableEmployesAsync(int projectId);
        Task DeleteEmployeeFromProjectAsync(int projectId, string employeId);
    }
}

﻿using Application.Services.Interfaces;
using Domain.Entities;
using Domain.Interfaces;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Implementations
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork uow;
        public EmployeeService(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        public Employee CreateEmployee(EmployeeCreateModel model)
        {
            return new Employee()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                MiddleName = model.MiddleName,
                Email = model.Email,
                UserName = model.Email,
                PhoneNumber = model.FirstName                
            };            
        }

        public async Task DeleteEmployeeAsync(string id)
        {
            await uow.EmployeeRepository.DeleteByIdAsync(id);
            await uow.SaveAsync();
        }

        public async Task EditEmployeeAsync(EmployeeCreateModel model)
        {
            var employee = await uow.EmployeeRepository.GetByIdAsync(model.Id);
            if (employee == null)
                throw new Exception();
            employee.FirstName = model.FirstName;
            employee.LastName = model.LastName;
            employee.MiddleName = model.MiddleName;
            employee.Email = model.Email;
            uow.EmployeeRepository.Update(employee);
            await uow.SaveAsync();
        }

        public List<Employee> GetAllEmployes()
        {
            return uow.EmployeeRepository.All.ToList();
        }

        public async Task<Employee> GetEmployeeByIdAsync(string id)
        {
            return await uow.EmployeeRepository.GetByIdAsync(id);
        }

        public EmployeeCreateModel GetEmployeeModel(Employee employee) 
        {
            return new EmployeeCreateModel()
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                MiddleName = employee.MiddleName,
                Email = employee.Email,
                Id = employee.Id
            };
        }

    }
}

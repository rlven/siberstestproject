﻿using Application.Services.Interfaces;
using Domain.Entities;
using Domain.Interfaces;
using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Implementations
{
    public class IssueService : IIssueService
    {
        private readonly IUnitOfWork uow;
        public IssueService(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        public async Task AddIssuesToProject(AddIssuesToProjectModel model)
        {
            var project = await uow.ProjectRepository.GetByIdAsync(model.ProjectId);
            if (project == null)
                throw new Exception();

            foreach (var issueId in model.IssueIds)
            {
                var issue = await uow.IssueRepository.GetByIdAsync(issueId);
                issue.ProjectId = project.Id;
                await uow.SaveAsync();
            }
            await uow.SaveAsync();
        }


        public Task ChangeIssueExecutor(int employeeId, int issueId)
        {
            throw new NotImplementedException();
        }

        public async Task CreateIssueAsync(Issue issue)
        {
            await uow.IssueRepository.AddAsync(issue);
            await uow.SaveAsync();
        }

        public async Task DeleteIssueAsync(int id)
        {
            await uow.IssueRepository.DeleteByIdAsync(id);
            await uow.SaveAsync();
        }

        public async Task DeleteIssueFromProject(int projectId, int issueId)
        {
            var project = await uow.ProjectRepository.GetByIdAsync(projectId);
            var issue = project.Issues.FirstOrDefault(x => x.Id == issueId);
            if (issue == null)
                throw new Exception();
            issue.ProjectId = null;
            await uow.SaveAsync();
        }

        public async Task EditIssueAsync(Issue issue)
        {
            var currIssue = await uow.IssueRepository.GetByIdAsync(issue.Id);
            if (currIssue == null)
                throw new Exception();
            currIssue.Name = issue.Name;
            currIssue.AuthorId = issue.AuthorId;
            currIssue.ExecutorId = issue.ExecutorId;
            currIssue.IssueStatusId = issue.IssueStatusId;
            currIssue.Description = issue.Description;
            currIssue.IssuePriority = issue.IssuePriority;
            uow.IssueRepository.Update(currIssue);
            await uow.SaveAsync();
        }

        public async Task<List<Issue>> GetAllIssuesAsync()
        {
            return await uow.IssueRepository.All.ToListAsync();
        }

        public async Task<Issue> GetIssueByIdAsync(int id)
        {
            return await uow.IssueRepository.GetByIdAsync(id);
        }

        public IQueryable<IssueStatus> GetIssueStatuses()
        {
            return uow.IssueStatusRepository.All;
        }
    }
}

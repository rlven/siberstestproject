﻿using Application.Services.Interfaces;
using Domain.Entities;
using Domain.Interfaces;
using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Implementations
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork uow;
        public ProjectService(IUnitOfWork uow) 
        {
            this.uow = uow;
        }       

        public async Task AddEmployesToProjectAsync(AddEmployesToProjectModel model)
        {
            var project = await uow.ProjectRepository.GetByIdAsync(model.ProjectId);
            if (project == null)
                throw new Exception();
            List<EmployeeProject> newProjectEmployes = new List<EmployeeProject>();
            foreach (var emp in model.EmployeIds) 
            {
                var newEmployeProject = new EmployeeProject()
                {
                    EmployeeId = emp,
                    ProjectId = model.ProjectId
                };
                newProjectEmployes.Add(newEmployeProject);
            }
            await uow.EmployeeProjectRepository.AddRangeAsync(newProjectEmployes);
            await uow.SaveAsync();
        }

        public async Task CreateProjectAsync(ProjectCreateEditModel model)
        {
            var newProject = new Project()
            {
                Name = model.Name,
                CustomerName = model.Customer,
                ExecutorName = model.Executor,
                StartDate = model.ProjectStart,
                EndDate = model.ProjectEnd,
                ProjectPriority = model.Priority,
                ProjectSupervisorId = model.SupervisorId
            };
            await uow.ProjectRepository.AddAsync(newProject);
            await uow.SaveAsync();
        }

        public async Task DeleteEmployeeFromProjectAsync(int projectId, string employeId)
        {
            var employeeProject = uow.EmployeeProjectRepository.All.FirstOrDefault(x =>
                                  x.EmployeeId == employeId && x.ProjectId == projectId);
            if (employeeProject == null)
                throw new Exception();
            await uow.EmployeeProjectRepository.DeleteByIdAsync(employeeProject.Id);
            await uow.SaveAsync();
        }


        public async Task DeleteProjectAsync(int id)
        {
            var project = await uow.ProjectRepository.DeleteByIdAsync(id);
            await uow.SaveAsync();
        }

        public async Task EditProjectAsync(Project model)
        {
            var project = await GetProjectByIdAsync(model.Id);
            if (project == null)
                throw new Exception();
            project.Name = model.Name;
            project.CustomerName = model.CustomerName;
            project.ExecutorName = model.ExecutorName;
            project.StartDate = model.StartDate;
            project.EndDate = model.EndDate;
            project.ProjectPriority = model.ProjectPriority;
            uow.ProjectRepository.Update(project);
            await uow.SaveAsync();
        }

        public List<Project> GetAllProjects()
        {
            return uow.ProjectRepository.All.ToList();
        }

        public async Task<List<Employee>> GetAvailableEmployesAsync(int projectId)
        {
            var projectEmployes = await uow.ProjectRepository.GetProjectEmployesAsync(projectId);
            var allEmployes = await uow.EmployeeRepository.All.ToListAsync();
            if (!projectEmployes.Any())
                return allEmployes;
            var availableEmployes = from emp in allEmployes
                                    where projectEmployes
                                    .Select(x=>x.Id).Contains(emp.Id) select emp;
            return availableEmployes.ToList();
        }

        public Task<Project> GetProjectByIdAsync(int id)
        {
            return uow.ProjectRepository.GetByIdAsync(id);
        }

        public async Task<Project> GetProjectDetailsAsync(int id)
        {
            return await uow.ProjectRepository.GetByIdAsync(id);
        }

    }
}

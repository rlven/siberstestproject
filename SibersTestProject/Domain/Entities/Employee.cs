﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Employee : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public virtual ICollection<EmployeeProject> Projects { get; set; }
        public virtual ICollection<Issue> ExecutingIssues { get; set; }
        public virtual ICollection<Issue> AuthoredIssues { get; set; }
        public virtual ICollection<Project> LeadingProjects { get; set; }
    }
}

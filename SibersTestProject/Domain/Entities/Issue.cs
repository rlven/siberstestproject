﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Issue : Entity<int>
    {
        public string Name { get; set; }
        [ForeignKey("Author")]
        public string AuthorId { get; set; }
        public virtual Employee Author { get; set; }
        [ForeignKey("Executor")]
        public string ExecutorId { get; set; }
        public virtual Employee Executor { get; set; }
        [ForeignKey("IssueStatus")]
        public int IssueStatusId { get; set; }
        public virtual IssueStatus IssueStatus { get; set; }
        public string Description { get; set; }
        public int IssuePriority { get; set; }
        [ForeignKey("Project")]
        public int? ProjectId { get; set; }
        public virtual Project Project { get; set; }
    }
}

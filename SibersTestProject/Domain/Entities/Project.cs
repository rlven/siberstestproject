﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Project : Entity<int>
    {
        public string Name { get; set; }
        public string CustomerName { get; set; }
        public string ExecutorName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ProjectPriority { get; set; }
        [ForeignKey("ProjectSupervisor")]
        public string ProjectSupervisorId { get; set; }
        public virtual Employee ProjectSupervisor { get; set; }
        public virtual ICollection<Issue> Issues { get; set; }
        public virtual ICollection<EmployeeProject> Employees { get; set; }

    }
}

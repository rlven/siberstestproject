﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class IssueStatus : Entity<int>
    {
        public string Name { get; set; }
        public virtual ICollection<Issue> Issues { get; set; }
    }
}

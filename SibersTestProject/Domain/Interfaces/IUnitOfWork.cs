﻿using Domain.Interfaces.Repositories;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IUnitOfWork
    {
        IProjectRepository ProjectRepository { get; }
        IEmployeeRepository EmployeeRepository { get;  }
        IEmployeeProjectRepository EmployeeProjectRepository { get; }
        IIssueRepository IssueRepository { get; }
        IIssueStatusRepository IssueStatusRepository { get; }
        void Save();
        Task SaveAsync();
    }
}

﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Repositories
{
    public interface IProjectRepository : IRepository<Project, int>
    {
        Task<IEnumerable<Employee>> GetProjectEmployesAsync(int id);
    }
}

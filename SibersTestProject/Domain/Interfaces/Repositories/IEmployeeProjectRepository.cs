﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces.Repositories
{
    public interface IEmployeeProjectRepository : IRepository<EmployeeProject, int>
    {
    }
}

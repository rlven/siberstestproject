﻿using Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class AppDbContext : IdentityDbContext<Employee>
    {
        public AppDbContext()
        {
        }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<Issue> Issues { get; set; }
        public virtual DbSet<EmployeeProject> EmployeeProjects { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IssueStatus>().HasData(
                new IssueStatus() { Id = 1, Name = "ToDo" },
                new IssueStatus() { Id = 2, Name = "InProgress" },
                new IssueStatus() { Id = 3, Name = "Done" });

            modelBuilder.Entity<Employee>()
                .HasMany(x => x.AuthoredIssues)
                .WithOne(z => z.Author);

            modelBuilder.Entity<Employee>()
                .HasMany(x => x.ExecutingIssues)
                .WithOne(z => z.Executor);

            base.OnModelCreating(modelBuilder);
        }
    }
}

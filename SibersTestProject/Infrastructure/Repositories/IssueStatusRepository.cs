﻿using Domain.Entities;
using Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Repositories
{
    public class IssueStatusRepository : Repository<IssueStatus, int>, IIssueStatusRepository
    {
        public IssueStatusRepository(AppDbContext context) : base(context)
        {

        }
    }
}

﻿using System.Threading.Tasks;
using Domain.Entities;
using Domain.Interfaces.Repositories;


namespace Infrastructure.Repositories
{
    public class EmployeProjectRepository : Repository<EmployeeProject, int>, IEmployeeProjectRepository
    {
        public EmployeProjectRepository(AppDbContext context) : base(context)
        {

        }
    }
}

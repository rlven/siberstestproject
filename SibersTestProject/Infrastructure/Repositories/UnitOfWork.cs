﻿using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext context;
        private IProjectRepository projectRepository;
        private IEmployeeRepository employeeRepository;
        private IEmployeeProjectRepository employeeProjectRepository;
        private IIssueRepository issueRepository;
        private IIssueStatusRepository issueStatusRepository;
        public UnitOfWork(AppDbContext context)
        {
            this.context = context;
        }
        public IProjectRepository ProjectRepository
        {
            get { return projectRepository ??= new ProjectRepository(context); }
        }
        public IIssueStatusRepository IssueStatusRepository
        {
            get { return issueStatusRepository ??= new IssueStatusRepository(context); }
        }
        public IIssueRepository IssueRepository
        {
            get { return issueRepository ??= new IssueRepository(context); }
        }
        public IEmployeeProjectRepository EmployeeProjectRepository
        {
            get { return employeeProjectRepository ??= new EmployeProjectRepository(context); }
        }

        public IEmployeeRepository EmployeeRepository
        {
            get { return employeeRepository ??= new EmployeeRepository(context); }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }
    }
}

﻿using Domain.Entities;
using Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Repositories
{
    public class IssueRepository : Repository<Issue, int>, IIssueRepository
    {
        public IssueRepository(AppDbContext context) : base(context)
        {

        }
    }
}

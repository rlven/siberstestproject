﻿using Domain.Entities;
using Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class ProjectRepository : Repository<Project, int>, IProjectRepository
    {
        private readonly AppDbContext context;
        public ProjectRepository(AppDbContext context) : base(context)
        {
            this.context = context;
        }

        public async  Task<IEnumerable<Employee>> GetProjectEmployesAsync(int id)
        {
            var project = await context.Projects.FindAsync(id);
            if(project!=null)
                return project.Employees?.Select(x => x.Employee);
            return null;
        }
    }
}

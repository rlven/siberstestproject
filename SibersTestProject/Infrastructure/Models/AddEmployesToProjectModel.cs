﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infrastructure.Models
{
    public class AddEmployesToProjectModel
    {
        [Required]
        public List<string> EmployeIds { get; set; }
        [Required]
        public int ProjectId { get; set; }
    }
}

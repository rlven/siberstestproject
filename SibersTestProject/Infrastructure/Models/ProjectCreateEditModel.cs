﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infrastructure.Models
{
    public class ProjectCreateEditModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Customer { get; set; }
        [Required]
        public string Executor { get; set; }
        [Required]
        public DateTime ProjectStart { get; set; }
        [Required]
        public DateTime ProjectEnd { get; set; }
        [Required]
        public int Priority { get; set; }
        public int Id { get; set; }
        [Required]
        public string SupervisorId { get; set; }
    }
}

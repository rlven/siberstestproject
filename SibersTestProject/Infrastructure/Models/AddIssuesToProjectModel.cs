﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Models
{
    public class AddIssuesToProjectModel
    {
        public List<int> IssueIds { get; set; }
        public int ProjectId { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class supervisor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProjectSupervisorId",
                table: "Projects",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Projects_ProjectSupervisorId",
                table: "Projects",
                column: "ProjectSupervisorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_AspNetUsers_ProjectSupervisorId",
                table: "Projects",
                column: "ProjectSupervisorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_AspNetUsers_ProjectSupervisorId",
                table: "Projects");

            migrationBuilder.DropIndex(
                name: "IX_Projects_ProjectSupervisorId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "ProjectSupervisorId",
                table: "Projects");
        }
    }
}

﻿$(function () {
    var arry = [];
    var projectId = $("#project-id").val();


    $('.form-checkbox').change(function () {
        var userId = $(this).closest('.table-row').find('.employe-id').text();
        var found = jQuery.inArray(userId, arry);
        if (this.checked) {
            arry.push(userId);
        }
        else {
            arry.splice(found, 1);
        }
        console.log(arry);
    });

    $(document).on('click', '#add-users-to-board-btn', function () {

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Project/AddEmployesToProject/',
            data: { EmployeIds: arry, ProjectId: projectId },
            success: function (data) {
                if (data.success) {
                    alert("success");
                    location.href = "/Project/GetProjectDetails/" + projectId
                } else {
                    alert(data.message);
                    $('.alert').alert();
                }
            }
        });
    });

    $(document).on('click', '#add-issue-to-board-btn', function () {

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Issue/AddIssueToProject/',
            data: { IssueIds: arry, ProjectId: projectId },
            success: function (data) {
                if (data.success) {
                    alert("success");
                    location.href = "/Project/GetProjectDetails/" + projectId
                } else {
                    alert(data.message);
                    $('.alert').alert();
                }
            }
        });
    });

    $(document).on('click', '#delete-users-from-board-btn', function () {

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/LeaderBoard/DeleteUsersFromBoard/',
            data: { userIds: arry, boardId: boardId },
            success: function (data) {
                if (data.success) {
                    alert("success");
                    location.href = "/Project/GetProjectDetails/" + projectId
                } else {
                    alert(data.message);
                    $('.alert').alert();
                }
            }
        });
    });

});
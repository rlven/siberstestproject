﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Domain.Entities;
using Infrastructure.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace SibersTestProject.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    public class EmployeeController : Controller
    {
        private readonly UserManager<Employee> userManager;
        private IEmployeeService employeService;
        public EmployeeController(UserManager<Employee> userManager, IEmployeeService employeService) 
        {
            this.userManager = userManager;
            this.employeService = employeService;
        }
        public IActionResult Index()
        {
            var employes = employeService.GetAllEmployes();
            return View(employes);
        }

        [HttpGet]
        public IActionResult CreateEmploye() 
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateEmploye(EmployeeCreateModel model) 
        {
            if (ModelState.IsValid) 
            {
                var employee = employeService.CreateEmployee(model);
                var result = await userManager.CreateAsync(employee, Guid.NewGuid().ToString());
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(CreateEmploye));
        }

        [HttpGet]
        public async Task<IActionResult> EditEmploye(string id)
        {
            var employee = await employeService.GetEmployeeByIdAsync(id);
            var model = employeService.GetEmployeeModel(employee);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditEmploye(EmployeeCreateModel model)
        {
            if (ModelState.IsValid) 
            {
                await employeService.EditEmployeeAsync(model);
                return RedirectToAction(nameof(Index));
            }

            return RedirectToAction(nameof(EditEmploye));
        }

        public async Task<IActionResult> DeleteEmploye(string id)
        {
            await employeService.DeleteEmployeeAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Domain.Entities;
using Infrastructure.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SibersTestProject.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    public class ProjectController : Controller
    {
        private IProjectService projectService;
        private IEmployeeService employeeService;
        public ProjectController(IProjectService projectService, IEmployeeService employeeService) 
        {
            this.projectService = projectService;
            this.employeeService = employeeService;
        }
        public IActionResult Index()
        {
            var projects = projectService.GetAllProjects();
            return View(projects);
        }

        [HttpGet]
        public IActionResult CreateProject()
        {
            ViewBag.Employes = GetEmployesList();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateProject(ProjectCreateEditModel model) 
        {
            if (ModelState.IsValid) 
            {
                await projectService.CreateProjectAsync(model);
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(CreateProject));
        }

        [HttpGet]
        public async Task<IActionResult> EditProject(int id) 
        {
            ViewBag.Employes = GetEmployesList();
            var project = await projectService.GetProjectByIdAsync(id);
            return View(project);
        }

        [HttpPost]
        public async Task<IActionResult> EditProject(Project project) 
        {
            if (ModelState.IsValid)             
            {
                await projectService.EditProjectAsync(project);
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(EditProject));
        }


        public async Task<IActionResult> DeleteProject(int id) 
        {
            await projectService.DeleteProjectAsync(id);
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> GetProjectDetails(int id)
        {
            var project = await projectService.GetProjectDetailsAsync(id);
            return View(project);
        }

        [HttpGet]
        public async Task<IActionResult> AddEmployesToProject(int projectId)
        {
            ViewBag.ProjectId = projectId;
            var projectEmployes = await projectService.GetAvailableEmployesAsync(projectId);
            return View(projectEmployes);
        }

        [HttpPost]
        public async Task<IActionResult> AddEmployesToProject(AddEmployesToProjectModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await projectService.AddEmployesToProjectAsync(model);
                    return Json(new { success = true });
                }
                catch (Exception e)
                {
                    return Json(new { success = false, error = e.Message });
                }
            }
            return Json(new { success = false, error = "Введите правильные данные" });
        }


        public async Task<IActionResult> DeleteEmployeeFromProject(int projectId, string employeId)
        {
            await projectService.DeleteEmployeeFromProjectAsync(projectId, employeId);
            return RedirectToAction(nameof(GetProjectDetails), new { id = projectId });
        }

        private IEnumerable<SelectListItem> GetEmployesList()
        {
            return new SelectList(employeeService.GetAllEmployes()
                .OrderBy(a => a.Id)
                .Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.FirstName })
                .ToList(), "Value", "Text");
        }
    }
}
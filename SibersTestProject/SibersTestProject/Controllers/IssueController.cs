﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Domain.Entities;
using Infrastructure.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SibersTestProject.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    public class IssueController : Controller
    {
        private IIssueService issueService;
        private IEmployeeService employeeService;
        private IProjectService projectService;
        public IssueController(IIssueService issueService, IEmployeeService employeeService, 
            IProjectService projectService) 
        {
            this.issueService = issueService;
            this.employeeService = employeeService;
            this.projectService = projectService;
        }
        public async Task<IActionResult> Index()
        {
            var issues = await issueService.GetAllIssuesAsync();
            return View(issues);
        }

        [HttpGet]
        public IActionResult CreateIssue()
        {
            ViewBag.Employes = GetEmployesList();
            ViewBag.IssueStatuses = GetIssueStatuses();
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetIssueDetails(int id)
        {
            var issue = await issueService.GetIssueByIdAsync(id);
            return View(issue);
        }
        public async Task<IActionResult> DeleteIssueFromProject(int projectId, int issueId) 
        {
            await issueService.DeleteIssueFromProject(projectId, issueId);
            return RedirectToAction("GetProjectDetails", "Project", new { id = projectId});
        }

        [HttpPost]
        public async Task<IActionResult> CreateIssue(Issue issue)
        {
            await issueService.CreateIssueAsync(issue);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> EditIssue(int id)
        {
            ViewBag.Employes = GetEmployesList();
            ViewBag.IssueStatuses = GetIssueStatuses();
            var issue = await issueService.GetIssueByIdAsync(id);
            return View(issue);
        }

        [HttpPost]
        public async Task<IActionResult> EditIssue(Issue issue)
        {
            await issueService.EditIssueAsync(issue);
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> DeleteIssue(int id)
        {
            await issueService.DeleteIssueAsync(id);
            return RedirectToAction(nameof(Index));
        }
        [HttpGet]
        public async Task<IActionResult> AddIssueToProject(int projectId) 
        {
            ViewBag.ProjectId = projectId;
            var issues = await issueService.GetAllIssuesAsync();
            return View(issues);
        }

        [HttpPost]
        public async Task<IActionResult> AddIssueToProject(AddIssuesToProjectModel model)
        {
            if (ModelState.IsValid)
            {
                await issueService.AddIssuesToProject(model);
                return Json(new { success = true });
            }
            return Json(new { success = false });

        }
        private IEnumerable<SelectListItem> GetEmployesList()
        {
            return new SelectList(employeeService.GetAllEmployes()
                .OrderBy(a => a.Id)
                .Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.FirstName })
                .ToList(), "Value", "Text");
        }
        private IEnumerable<SelectListItem> GetIssueStatuses()
        {
            return new SelectList(issueService.GetIssueStatuses()
                .OrderBy(a => a.Id)
                .Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.Name })
                .ToList(), "Value", "Text");
        }
    }
}